<?php

namespace App;

class Translate {
    function translate(string $input): string
    {
        $result = '';
        $start = 0;
        $end = 0;
        $isWord = false;
        $inputSize = mb_strlen($input);
        for ($i = 0; $i < $inputSize; $i++) {
            $char = mb_substr($input, $i, 1);
            if (!$isWord && $this->is_word_char($char)) {
                $isWord = true;
                $start = $i;
                $end = $i + 1;
                continue;
            }
            if ($isWord && $this->is_word_char($char)) {
                $end = $i + 1;
                continue;
            }
            if ($isWord && !$this->is_word_char($char)) {
                $isWord = false;
                $word = mb_substr($input, $start, $end - $start);
                $reversedWord = $this->mb_strrev($word);
                $reversedWord = $this->mb_str_adjust_case($reversedWord, $word);
                $result .= $reversedWord;
            }
            if (!$isWord && !$this->is_word_char($char)) {
                $result .= $char;
            }
        }

        if ($isWord) {
            $word = mb_substr($input, $start, $end - $start);
            $reversedWord = $this->mb_strrev($word);
            $reversedWord = $this->mb_str_adjust_case($reversedWord, $word);
            $result .= $reversedWord;
        }

        return $result;
    }

    function is_word_char($char): bool {
        $result = preg_match('/\\p{L}/', $char);
        return $result !== false && $result > 0;
    }

    function mb_strrev(string $string): string {
        $stringSize = mb_strlen($string);
        $reversedString = '';
        for ($i = 0; $i < $stringSize; $i++) {
            $char = mb_substr($string, $i, 1);
            $reversedString = $char.$reversedString;
        }
        return $reversedString;
    }

    function mb_str_adjust_case(string $string, string $pattern): string {
        $result = '';
        $stringSize = mb_strlen($string);
        for ($i = 0; $i < $stringSize; $i++) {
            $stringChar = mb_substr($string, $i, 1);
            $patternChar = mb_substr($pattern, $i, 1);

            if ($this->mb_ctype_lower($stringChar) && $this->mb_ctype_upper($patternChar)) {
                $stringChar = mb_strtoupper($stringChar);
            } else if ($this->mb_ctype_upper($stringChar) && $this->mb_ctype_lower($patternChar)) {
                $stringChar = mb_strtolower($stringChar);
            }

            $result .= $stringChar;
        }

        return $result;
    }

    function mb_ctype_lower(string $string): bool {
        return $string === mb_strtolower($string);
    }

    function mb_ctype_upper(string $string): bool {
        return $string === mb_strtoupper($string);
    }
}