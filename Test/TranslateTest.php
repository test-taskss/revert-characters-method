<?php

use App\Translate;
use PHPUnit\Framework\TestCase;

class TranslateTest extends TestCase
{
    protected Translate $translate;

    protected function setUp(): void
    {
        $this->translate = new Translate();
    }

    /**
     * @dataProvider dataProvider
     */
    public function test($input, $expected)
    {
        $actual = $this->translate->translate($input);
        $this->assertSame($expected, $actual);
    }

    public function dataProvider(): array
    {
        return [
            ['имя', 'ями'],
            ['Вася', 'Ясав'],
            ['Здравствуйте! Меня зовут имя. имячи', 'Етйувтсвардз! Янем тувоз ями. ичями'],
            ['Привет! Давно не виделись.', 'Тевирп! Онвад ен ьсиледив.']
        ];
    }
}